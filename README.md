# Hello

This is the Presentation backing the repo at https://arno.gitlab.io/your-blog

Feel free to view the code at https://gitlab.com/arno/your-blog-presi

## Contributing

The whole thing is build on top of reveal.js. The markup can be found in `index.html`, and the essential parts of styling are in `css/your-blog.css`.

If there are any issues, feel free to [open an issue](https://gitlab.com/arno/your-blog-presi/issues/new) or fork this repo, make fixes and [open a Merge Request](https://gitlab.com/arno/your-blog-presi/merge_requests/new).

## License of added material

All assets added by me (Arno Fleming) are under MIT license.
